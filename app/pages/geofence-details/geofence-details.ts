import { Component } from "@angular/core";
import { NavController, NavParams, MenuController } from "ionic-angular";
import * as Leaflet from "leaflet";

@Component({
  templateUrl: "build/pages/geofence-details/geofence-details.html"
})
export class GeofenceDetailsPage {
  private geofence: Geofence;
  private _radius: number;
  private _latLng: any;
  private notificationText: string;
  private transitionType: string;
  private circle: any;
  private marker: any;
  private map: any;

  constructor(
    private nav: NavController
  ) {
    this._radius = 3000;
    this._latLng = Leaflet.latLng(0, 0);    
  }

  newLatLng() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this._latLng = Leaflet.latLng(
          position.coords.latitude, 
          position.coords.longitude
        );
        setTimeout(this.loadMap.bind(this), 100);
      },
      (error) => {
        this._latLng = Leaflet.latLng(0, 0);
        setTimeout(this.loadMap.bind(this), 100);
      },
      { timeout: 5000 }
    );
  }

  get radius() {
    return this._radius;
  }

  set latLng(value) {
    this._latLng = value;
    this.circle.setLatLng(value);
    this.marker.setLatLng(value);
  }

  get latLng() {
    return this._latLng;
  }

  ionViewLoaded() {
    this.newLatLng();
  }

  loadMap() {
    this.map = Leaflet
      .map("map")
      .setView(this.latLng, 13)
      .on("click", this.onMapClicked.bind(this))

    Leaflet.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png")
      .addTo(this.map);

    this.marker = Leaflet
      .marker(this.latLng, { draggable: true })
      .on("dragend", this.onMarkerPositionChanged.bind(this))
      .addTo(this.map);

    this.circle = Leaflet.circle(this.latLng, this.radius).addTo(this.map);
  }

  onMapClicked(e) {
    this.latLng = e.latlng;
  }

  onMarkerPositionChanged(e) {
    const latlng = e.target.getLatLng();

    this.latLng = latlng;
  }

}
