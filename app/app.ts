import { Component, ViewChild } from "@angular/core";
import { ionicBootstrap, Nav, Platform, Alert, MenuController } from "ionic-angular";
import { GeofenceDetailsPage } from "./pages/geofence-details/geofence-details";
import * as Leaflet from "leaflet";

@Component({
  templateUrl: "build/app.html"
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = GeofenceDetailsPage;

  constructor(
    platform: Platform
  ) {
    platform.ready().then(() => {
      Leaflet.Icon.Default.imagePath = "img";
    });
  }

}

// Pass the main app component as the first argument
// Pass any providers for your app in the second argument
// Set any config for your app as the third argument:
// http://ionicframework.com/docs/v2/api/config/Config/

ionicBootstrap(MyApp, [], {});
