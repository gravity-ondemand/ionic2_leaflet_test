# Ionic2 leaflet sample project

## Installation

Use local npm packages

```
npm install
```

If you're using `ionic-cli` installed globally not locally you should install the latest beta version in order to run this project.
```
npm install -g ionic@beta
```

For testing in the browser

```
ionic serve
```

For android

```
ionic platform add android
ionic run android
```

For iOS

```
ionic platform add ios
ionic run ios
```